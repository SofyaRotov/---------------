#ifndef LLINE_H
#define LLINE_H

#include "Brick.h"

class LLine : public Brick
{
public:
	LLine()
		: Brick(Brick::L)
	{
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			brickSprite[i].setColor(Color(0, 0, 255));		//Blue
			this->directionFacing = Brick::North;

			brickSprite[i].setTexture(brickTexture);
			isSpriteMarkedForDeletion[i] = false;
		}
		setInitialPositions();
	}

	void setInitialPositions()
	{
		setPosition(0, coord_t(4, 0));
		setPosition(1, coord_t(4, 1));
		setPosition(2, coord_t(4, 2));
		setPosition(3, coord_t(5, 2));
	}

	coord_t(&previewRotate())[SPRITES_IN_BRICK]
	{

		switch (directionFacing)
		{
		case Brick::West:
		{
			previewDirectionFacing = Brick::North;
			previewRotateCoords[0] = calculateNewPosition(0, 1, -3);
			previewRotateCoords[1] = calculateNewPosition(1, 0, -2);
			previewRotateCoords[2] = calculateNewPosition(2, -1, -1);
			break;
		}

		case Brick::North:
		{
			previewDirectionFacing = Brick::East;
			previewRotateCoords[0] = calculateNewPosition(0, 3, 1);
			previewRotateCoords[1] = calculateNewPosition(1, 2, 0);
			previewRotateCoords[2] = calculateNewPosition(2, 1, -1);
			break;
		}

		case Brick::East:
		{
			previewDirectionFacing = Brick::South;
			previewRotateCoords[0] = calculateNewPosition(0, -1, 3);
			previewRotateCoords[1] = calculateNewPosition(1, 0, 2);
			previewRotateCoords[2] = calculateNewPosition(2, 1, 1);
			break;
		}

		case Brick::South:
		{
			previewDirectionFacing = Brick::West;
			previewRotateCoords[0] = calculateNewPosition(0, -3, -1);
			previewRotateCoords[1] = calculateNewPosition(1, -2, 0);
			previewRotateCoords[2] = calculateNewPosition(2, -1, 1);
			break;
		}
		}
		previewRotateCoords[3] = getPosition(3);
		isPreviewCalculated = true;
		return previewRotateCoords;
	}
};

#endif