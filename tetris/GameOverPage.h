#ifndef GAME_OVER_PAGE_H
#define GAME_OVER_PAGE_H

#include "Page.h"


class GameOverPage : public Page
{
public:
	GameOverPage()
		: Page()
	{
		std::shared_ptr<sf::Font> font = std::make_shared<sf::Font>(); //������ ����� 
		font->loadFromFile("Images/arial.ttf"); //��������� �� ����� 
		m_objectsManager.add<sf::Font>("font", font); //��������� �� ���������

		std::shared_ptr<sf::Text> gameOverScoreHeaderText = std::make_shared<sf::Text>("Score: ", *font); //����� ��� ����� 
		std::shared_ptr<sf::Text> gameOverScoreDisplayText = std::make_shared<sf::Text>(); //������ ������, ��� �������� ���-�� �����
		gameOverScoreDisplayText->setFont(*font); //����� �����
		std::shared_ptr<sf::Text> gameOverLevelHeaderText = std::make_shared<sf::Text>("Level: ", *font); //�� �� ����� ��� ������
		std::shared_ptr<sf::Text> gameOverLevelDisplayText = std::make_shared<sf::Text>();
		gameOverLevelDisplayText->setFont(*font);
		std::shared_ptr<sf::Text> gameOverHeaderText = std::make_shared<sf::Text>("\t  GAME OVER!\nHere are your final stats:", *font); //������ ���� ������

		gameOverScoreHeaderText->setCharacterSize(TEXT_CHAR_SIZE); //������ ��������
		gameOverScoreHeaderText->setPosition(130, 320); ////������� ������
		m_objectsManager.add<sf::Text>("scoreHeaderText", gameOverScoreHeaderText);//��������� �� ���������
		gameOverScoreDisplayText->setCharacterSize(TEXT_CHAR_SIZE); //��� ����� 
		gameOverScoreDisplayText->setPosition(210, 320);
		m_objectsManager.add<sf::Text>("scoreDisplayText", gameOverScoreDisplayText);
		gameOverLevelHeaderText->setCharacterSize(TEXT_CHAR_SIZE); //���� ��������� Level
		gameOverLevelHeaderText->setPosition(130, 350);
		m_objectsManager.add<sf::Text>("levelHeaderText", gameOverLevelHeaderText);
		gameOverLevelDisplayText->setCharacterSize(TEXT_CHAR_SIZE); //��� ����������� ������ 
		gameOverLevelDisplayText->setPosition(210, 350);
		m_objectsManager.add<sf::Text>("levelDisplayText", gameOverLevelDisplayText);
		gameOverHeaderText->setCharacterSize(TEXT_CHAR_SIZE); //�������� ���������
		gameOverHeaderText->setPosition(210, 265);
		m_objectsManager.add<sf::Text>("headerText", gameOverHeaderText);

		std::shared_ptr<sf::Texture> gameOverBackground = std::make_shared<sf::Texture>(); //������ ������ ���
		gameOverBackground->loadFromFile("Images/gameOverBG.png"); //��������� ��� �� ����� 
		m_objectsManager.add<sf::Texture>("background", gameOverBackground); //��������� ��� �� �������� 

		std::shared_ptr<sf::Sprite> gameOverBackgroundSprite = std::make_shared<sf::Sprite>(); //������ ���������
		gameOverBackgroundSprite->setTexture(*gameOverBackground); //��������� ���� �������� �� ����� 
		gameOverBackgroundSprite->setPosition(110, 82); //�������
		m_objectsManager.add<sf::Sprite>("backgroundSprite", gameOverBackgroundSprite); //��������� ��� �� �������� 
	}

	void draw()
	{
		//���� �� ���������� ������, �� �������
		if (!m_renderWindow) return;
		//������������ ��� ������� 
		m_renderWindow->draw(*getObject<sf::Sprite>("backgroundSprite"));
		m_renderWindow->draw(*getObject<sf::Text>("headerText"));
		m_renderWindow->draw(*getObject<sf::Text>("scoreHeaderText"));
		m_renderWindow->draw(*getObject<sf::Text>("scoreDisplayText"));
		m_renderWindow->draw(*getObject<sf::Text>("levelHeaderText"));
		m_renderWindow->draw(*getObject<sf::Text>("levelDisplayText"));
	}

	void processGameEvent(const sf::Event& gameEvent, GameState& state)
	{
		if (!m_renderWindow) return;
		if (gameEvent.type == Event::Closed || gameEvent.key.code == Keyboard::Escape) //���� ������ �� ������� ��� esc, �� �������
		{
			m_renderWindow->close(); //��������� ������
		}
	}
};


#endif