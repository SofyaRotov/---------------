#ifndef GAME_H
#define GAME_H

#include <SFML/System.hpp>
#include "Defines.h"
#include <vector>
#include <queue>
#include <random>
#include <chrono>

class TetrisLogic
{
public:
	TetrisLogic()
		: m_isCheckingLockTime(false)
		, m_score(0)
		, m_level(1)
		, m_totalLines(0)
	{
		resetGameBoard(); //�������� ������� ����
		refillBrickQueue(); //��������� ������� ����.�������
		m_activeBrick = getNextBrick(); //�������� �� ������� �����
		m_activeBrick->setActivePosition();
		m_previewBrick = getNextBrick();
		m_previewBrick->setPreviewPosition();
	}
	int getScore()
	{
		return m_score;
	}
	int getLevel()
	{
		return m_level;
	}
	Brick::Ptr getActiveBrick()
	{
		return m_activeBrick;
	}
	Brick::Ptr getPreviewBrick()
	{
		return m_previewBrick;
	}
	const std::vector<Brick::Ptr>& getBricks() //������, ������� ������ ������� ������
	{
		return m_brickHistoryList;
	}
	void processGame(GameState& state)
	{
		if (m_activeBrick->isLocked()) //���� ��������������
		{
			processLockedBrick(state, m_activeBrick);
			m_activeBrick = m_previewBrick;
			m_activeBrick->setActivePosition();
			m_previewBrick = getNextBrick();
			m_previewBrick->setPreviewPosition();
		}
		else
		{
			processBrickMotion(m_activeBrick);
		}
	}

	void checkInput(const Event& gameEvent)
	{
		Keyboard::Key keyPressed = gameEvent.key.code;

		switch (keyPressed)
		{
		case Keyboard::Left: //���� ������ �����
		{
			if (isSideCollision(m_activeBrick->getPosition(), true, false)) return;
			for (int i = 0; i < SPRITES_IN_BRICK; i++)
			{
				m_activeBrick->setPosition(i, m_activeBrick->calculateNewPosition(i, -1, 0));
			}
			break;
		}
		case Keyboard::Right: //������
		{
			if (isSideCollision(m_activeBrick->getPosition(), false, true)) return;
			for (int i = 0; i < SPRITES_IN_BRICK; i++)
			{
				m_activeBrick->setPosition(i, m_activeBrick->calculateNewPosition(i, 1, 0));
			}
			break;
		}
		case Keyboard::Down: //����
		{
			if (isBottomCollision(m_activeBrick->getPosition()))
			{
				m_activeBrick->lockBrick();
				return;
			}
			for (int i = 0; i < SPRITES_IN_BRICK; i++)
			{
				m_activeBrick->setPosition(i, m_activeBrick->calculateNewPosition(i, 0, 1));
			}

			m_score++;
			break;
		}
		case Keyboard::Z: //������ �� z
		{


			if (!isRotationCollision(m_activeBrick->previewRotate())) //�������� ��������������� ���������
			{

				m_activeBrick->rotate();

			}
			break;
		}
		}
	}
private:
	void processLockedBrick(GameState& state, Brick::Ptr activeBrick)
	{
		m_brickHistoryList.push_back(activeBrick);        //������� � ������ �������
		if (isTopCollision(activeBrick->getPosition()))  //�������� �� ������� �������
		{
			state = GameState::GameOver;
			return;
		}
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			m_gameBoard[activeBrick->getPosition(i).x][activeBrick->getPosition(i).y] = true; //true ��� ������� ������� �������
		}
		calculateScore(); //���-�� �����, ������� �� ��������
	}
	void processBrickMotion(Brick::Ptr activeBrick) //������������ ������� ������
	{
		Time fallTimer = m_brickFallTimer.getElapsedTime();
		Time lockTimer = m_brickLockTimer.getElapsedTime();
		float dropSpeed = 1 - (float)(m_level * 0.05f); //�������� � ����������� �� ������

		if (!isBottomCollision(activeBrick->getPosition()) && fallTimer.asSeconds() >= dropSpeed) //���� �� ����������� � �������� ��� �������
		{
			for (int i = 0; i < SPRITES_IN_BRICK; i++)
			{
				activeBrick->setPosition(i, activeBrick->calculateNewPosition(i, 0, 1)); //������������� �������
			}
			m_brickFallTimer.restart(); //������������� ������
			m_isCheckingLockTime = false;

		}
		else if (isBottomCollision(activeBrick->getPosition())) //���� �����������
		{
			if (!m_isCheckingLockTime)
			{
				m_isCheckingLockTime = true;
				m_brickLockTimer.restart();
			}
			else if (lockTimer.asSeconds() >= 0.5f)
			{
				activeBrick->lockBrick();
				m_isCheckingLockTime = false;
			}
		}
	}
	void resetGameBoard() //�������� ������� ����
	{
		for (size_t i = 0; i < GB_WIDTH; i++)
		{
			for (size_t j = 0; j < GB_HEIGHT; j++)
			{
				m_gameBoard[i][j] = false;
			}
		}
	}
	void calculateScore()
	{
		int linesCleared = 0;
		//��������� ����� � ������ �����, ������� ������ ���� ��������
		for (int i = BRICK_UPPER_BOUNDRY; i < GB_HEIGHT; i++)
		{
			for (int j = 0; j < GB_WIDTH; j++)
			{
				if (m_gameBoard[j][i])
				{
					if (j == GB_WIDTH - 1)
					{
						clearRow(i);
						linesCleared++;
						m_totalLines++;
						if (m_totalLines % 8 == 0 && m_level < LEVEL_MAX)
						{
							m_level++;
						}
					}
				}
				else
				{
					break;
				}
			}
		}
		//���������� �����, ������� �� ������ ��������
		if (linesCleared > 0)
		{
			if (linesCleared > 1 && linesCleared < 4)
			{
				m_score += (linesCleared * MULTI_LINE_CLEAR_SCORE) + BRICK_LOCK_SCORE;
			}
			else if (linesCleared == 4)
			{
				m_score += TETRIS_CLEAR_SCORE + BRICK_LOCK_SCORE;
			}
			else
			{
				m_score += SINGLE_LINE_CLEAR_SCORE + BRICK_LOCK_SCORE;
			}
		}
		else
		{
			m_score += BRICK_LOCK_SCORE;
		}
	}
	void refillBrickQueue() //��������� ������� ��������� �������
	{
		vector<Brick::Ptr> tempQueue;
		for (size_t i = 0; i < BRICKS_IN_QUEUE; i++)
		{
			for (size_t j = 0; j < NUMBER_OF_BRICK_TYPES; j++)
			{
				for (size_t k = 0; k < 2; k++)
				{
					Brick::Ptr newBrick = Brick::create((Brick::BrickType)j);
					tempQueue.push_back(newBrick);
					i++;
				}
			}
		}
		auto seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::shuffle(tempQueue.begin(), tempQueue.end(), std::default_random_engine(seed)); //����������� 14 ��������� ���������� � �������
		for (size_t i = 0; i < tempQueue.size(); i++) //����������� � ������� ��������
		{
			m_brickQueue.push(tempQueue[i]);
		}
	}
	Brick::Ptr getNextBrick() //���� ������� ������, �� ����� ��������� 
	{
		if (m_brickQueue.empty())
		{
			refillBrickQueue();
		}
		Brick::Ptr brick = m_brickQueue.front();
		m_brickQueue.pop();
		return brick;
	}
	bool isBottomCollision(const Brick::coord_t(&position)[SPRITES_IN_BRICK])
	{
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			int xPos = position[i].x;
			int yPos = position[i].y;

			if (yPos == BRICK_LOWER_BOUNDRY || m_gameBoard[xPos][yPos + 1])
			{
				return true;
			}
		}

		return false;
	}
	bool isTopCollision(const Brick::coord_t(&position)[SPRITES_IN_BRICK]) //���������� ������� �������
	{
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			int yPos = position[i].y;
			if (yPos < BRICK_UPPER_BOUNDRY)
			{
				return true;
			}
		}
		return false;
	}
	bool isSideCollision(const Brick::coord_t(&position)[SPRITES_IN_BRICK], bool left, bool right) //��������� �� ���� ��������� ������ � ��������� ��������� ��� �� ��������������� ������� � ���� �� �� �����, ���� �� ��� ���������, ������
	{
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			int xPos = position[i].x;
			int yPos = position[i].y;

			if (right && (xPos == BRICK_RIGHT_BOUNDRY || m_gameBoard[xPos + 1][yPos]) ||
				left && (xPos == BRICK_LEFT_BOUNDRY || m_gameBoard[xPos - 1][yPos]))
			{
				return true;
			}
		}
		return false;
	}
	bool isRotationCollision(Brick::coord_t(&position)[SPRITES_IN_BRICK]) //����� �� �� ������� ���� (��������)
	{
		for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
		{
			if (position[i].x < BRICK_LEFT_BOUNDRY || position[i].x > BRICK_RIGHT_BOUNDRY
				|| position[i].y > BRICK_LOWER_BOUNDRY
				|| m_gameBoard[position[i].x][position[i].y])
			{
				return true;
			}
		}
		return false;
	}
	void clearRow(size_t row) //�������� ����� �������� ������ 
	{
		for (size_t i = 0; i < GB_WIDTH; i++)
		{
			m_gameBoard[i][row] = false;
		}
		for (size_t i = 0; i < m_brickHistoryList.size(); i++)
		{
			Brick::Ptr currentBrick = m_brickHistoryList.at(i);
			for (size_t j = 0; j < SPRITES_IN_BRICK; j++)
			{
				if (currentBrick->getPosition(j).y == row)
				{
					currentBrick->markSpriteForDeletion(j);
					currentBrick->removeSprite(j);
				}
				if (currentBrick->getPosition(j).y < int(row) && !currentBrick->getSpriteDeletionStatus(j))
				{
					m_gameBoard[currentBrick->getPosition(j).x][currentBrick->getPosition(j).y] = false;
					currentBrick->setPosition(j, currentBrick->calculateNewPosition(j, 0, 1));
				}
			}
		}
		for (size_t i = 0; i < m_brickHistoryList.size(); i++) //��� �������������� �������� ����
		{
			Brick::Ptr currentBrick = m_brickHistoryList.at(i);

			for (size_t j = 0; j < SPRITES_IN_BRICK; j++)
			{
				if (!currentBrick->getSpriteDeletionStatus(j))
				{
					m_gameBoard[currentBrick->getPosition(j).x][currentBrick->getPosition(j).y] = true;
				}
			}
		}
	}
private:
	bool m_gameBoard[GB_WIDTH][GB_HEIGHT];
	bool m_isCheckingLockTime;
	size_t m_score;
	size_t m_level;
	size_t m_totalLines;
	std::queue<Brick::Ptr> m_brickQueue; //������� ��������� �������
	std::vector<Brick::Ptr> m_brickHistoryList; //������, ������� �����-���� ������ (��� ���������)
	Clock m_brickLockTimer;
	Clock m_brickFallTimer;
	Brick::Ptr m_activeBrick;
	Brick::Ptr m_previewBrick;
};



#endif