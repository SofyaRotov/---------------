#ifndef BRICK_FACTORY_H
#define BRICK_FACTORY_H

#include "Brick.h"
#include "Box.h"
#include "Line.h"
#include "lLine.h"
#include "tLine.h"
#include "zLine.h"
#include "ReverseLLine.h"
#include "ReverseZLine.h"

Brick::Ptr Brick::create(BrickType type)
{
	switch (type)
	{
	case Box:
		return std::make_shared<BoxBrick>();
	case Line:
		return std::make_shared<SimpleLine>();
	case Z:
		return std::make_shared<ZLine>();
	case ReverseZ:
		return std::make_shared<ReverseZLine>();
	case L:
		return std::make_shared<LLine>();
	case ReverseL:
		return std::make_shared<ReverseLLine>();
	case T:
		return std::make_shared<TLine>();
	default:
		return std::make_shared<BoxBrick>();
	}
}

#endif