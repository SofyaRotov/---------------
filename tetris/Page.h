#ifndef PAGE_H
#define PAGE_H

#include "Defines.h"
#include <SFML/Graphics.hpp>
#include <memory>
#include <map>

class IObject //���������� ��� ��������� �������� �� ��������������
{
public:
	typedef std::shared_ptr<IObject> Ptr; //����� ��������� (��� �������������� �������������� ��������)
	virtual ~IObject()
	{}
};


template <typename T> //������
					  //��������� ��� �������� SFML-��������, �������� sf::text sf::sprite
class ObjectSFML : public IObject
{
public:
	ObjectSFML(std::shared_ptr<T> object)
		: m_object(object)
	{}
	T* getObject()
	{
		return m_object.get();
	}
	static IObject::Ptr create(std::shared_ptr<T> object)
	{
		return std::make_shared<ObjectSFML<T> >(object);
	}
	virtual ~ObjectSFML() {}
private:
	std::shared_ptr<T> m_object;
};


class ObjectsManager //������ �������, ������������� - ������ 
{
public:
	template <typename T>
	void add(const std::string& tag, std::shared_ptr<T> objectSFML) //��������� ����� ������ � �������
	{

		//���� ��� ��� ������ �������� � ���� �������
		if (m_objects.find(tag) == m_objects.end())
		{
			IObject::Ptr object = ObjectSFML<T>::create(objectSFML); //������ ������
			m_objects[tag] = object; //������� � �������
		}
	}
	template <typename T>
	T* getObject(const std::string& tag) //���� �� ������� ������� �� �������������� 
	{
		auto object = m_objects.find(tag); //���� ������ 
		if (object != m_objects.end()) //���� ������ ����������, �� ��� ���������� 
		{
			ObjectSFML<T>* objectSFML = static_cast<ObjectSFML<T>*>(object->second.get());
			return objectSFML ? objectSFML->getObject() : nullptr; //���� �� ����������, ���������� ������� ��������� 
		}
		return nullptr;
	}
	~ObjectsManager()
	{}
private:
	std::map<std::string, IObject::Ptr> m_objects; //������� �������� � ���������� ���������������
};

class Page //����������� ��������, ������� ����� ��������� ��� ������ �������
{
public:
	Page()
		: m_renderWindow(nullptr) //�����������
	{}
	virtual ~Page()
	{
		m_renderWindow = nullptr; //����������
	}
	virtual void setRenderWindow(sf::RenderWindow* render) //������������� ���������� ������ ��� ���������
	{
		m_renderWindow = render;
	}
	virtual void processGameEvent(const sf::Event& gameEvent, GameState& state) = 0; //������������ ������� ������� � ���������� ������� �����
	virtual void draw() = 0; //������������ 

	template <typename T>
	T* getObject(const std::string& tag)
	{
		return m_objectsManager.getObject<T>(tag); //���������� ������� �� ����������� ��������������
	}

protected:
	sf::RenderWindow* m_renderWindow; //��������� �� ���������� ������ ��� ���������
	ObjectsManager m_objectsManager;  //��� �������� ��������, ������� ���������� ��� ���������� ���������

};



#endif