#ifndef TLINE_H
#define TLINE_H

#include "Brick.h"

class TLine : public Brick
{
public:
	TLine()
		: Brick(Brick::ReverseL)
	{
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			brickSprite[i].setColor(Color(0, 128, 128));	//Cyan
			this->directionFacing = Brick::North;

			brickSprite[i].setTexture(brickTexture);
			isSpriteMarkedForDeletion[i] = false;
		}
		setInitialPositions();
	}

	void setInitialPositions()
	{
		setPosition(0, coord_t(3, 1));
		setPosition(1, coord_t(4, 1));
		setPosition(2, coord_t(5, 1));
		setPosition(3, coord_t(4, 2));
	}

	coord_t(&previewRotate())[SPRITES_IN_BRICK]
	{

		switch (directionFacing)
		{
		case Brick::West:
		{
			previewDirectionFacing = Brick::North;
			previewRotateCoords[0] = calculateNewPosition(0, 0, -2);
			previewRotateCoords[1] = calculateNewPosition(1, 1, -1);
			previewRotateCoords[2] = calculateNewPosition(2, 2, 0);
			break;
		}

		case Brick::North:
		{
			previewDirectionFacing = Brick::East;
			previewRotateCoords[0] = calculateNewPosition(0, 2, 0);
			previewRotateCoords[1] = calculateNewPosition(1, 1, 1);
			previewRotateCoords[2] = calculateNewPosition(2, 0, 2);
			break;
		}
		case Brick::East:
		{
			previewDirectionFacing = Brick::South;
			previewRotateCoords[0] = calculateNewPosition(0, 0, 2);
			previewRotateCoords[1] = calculateNewPosition(1, -1, 1);
			previewRotateCoords[2] = calculateNewPosition(2, -2, 0);
			break;
		}
		case Brick::South:
		{
			previewDirectionFacing = Brick::West;
			previewRotateCoords[0] = calculateNewPosition(0, -2, 0);
			previewRotateCoords[1] = calculateNewPosition(1, -1, -1);
			previewRotateCoords[2] = calculateNewPosition(2, 0, -2);
			break;
		}
		}
		previewRotateCoords[3] = getPosition(3);
		isPreviewCalculated = true;
		return previewRotateCoords;
	}
};

#endif