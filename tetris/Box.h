#ifndef BOX_H
#define BOX_H

#include "Brick.h"

class BoxBrick : public Brick
{
public:
	BoxBrick()
		: Brick(Brick::Box)
	{
		for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
		{
			brickSprite[i].setColor(Color(255, 162, 21));	//Orange
			directionFacing = Brick::NullDirection;

			brickSprite[i].setTexture(brickTexture);
			isSpriteMarkedForDeletion[i] = false;
		}

		setInitialPositions();
	}

	void setInitialPositions()
	{
		setPosition(0, coord_t(4, 1));
		setPosition(1, coord_t(4, 2));
		setPosition(2, coord_t(5, 1));
		setPosition(3, coord_t(5, 2));
	}

	coord_t(&previewRotate())[SPRITES_IN_BRICK]
	{
		previewRotateCoords[0] = getPosition(0);
		previewRotateCoords[1] = getPosition(1);
		previewRotateCoords[2] = getPosition(2);
		previewRotateCoords[3] = getPosition(3);
		isPreviewCalculated = true;
		return previewRotateCoords;
	}

};


#endif