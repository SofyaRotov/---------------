#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/Window/Mouse.hpp>

#include "BrickFactory.h"
#include "Defines.h"
#include "MenuPage.h"
#include "GameOverPage.h"
#include "GamePage.h"
#include "Game.h"


int main()
{
	GameState state = GameState::MainMenu; // ����������� �����
	sf::VideoMode videoMode(SCREEN_WIDTH, SCREEN_HEIGHT); // ����� ������ � �����
	sf::RenderWindow renderWindow(videoMode, "SFML Tetris"); // ����, � ������� �� �������������� 

	sf::Event gameEvent; //������� ������� � ���� 
	MenuPage mainMenu; // ��������� �������� ����
	mainMenu.setRenderWindow(&renderWindow); //������������� ���������� ����, � ������� ������������ ������� ����
	GameOverPage gameOver;
	gameOver.setRenderWindow(&renderWindow);
	GamePage game;
	game.setRenderWindow(&renderWindow);

	while (renderWindow.isOpen()) //���� ������� ����, ������������ ������� �������
	{
		switch (state)
		{
		case GameState::MainMenu:
		{
			//pollEvent - �������, ������� �������� �����-�� ������� �� ������������
			// � ��������� true ��� false
			while (renderWindow.pollEvent(gameEvent)) //���� ���� ������� �� ������������, ������� ���� ������������ ��� �������
			{
				mainMenu.processGameEvent(gameEvent, state); //���� ��������� 
			}
			mainMenu.draw(); //����������
			renderWindow.display(); //��������
			break;
		}

		case GameState::GameOver:
		{
			auto score = gameOver.getObject<Text>("scoreDisplayText"); //���� �� gameover ������, ������� �������� �� ����� � ������
			score->setString(game.getScore()); //�� ��������� � ����� �������� ����� � ������ � ������ ��� � ���� ������
			auto level = gameOver.getObject<Text>("levelDisplayText"); // �� �� ����� � ������� (������ �������� �� ��������������, ������� �������� �� ���������� ������ �� ���� ���������)
			level->setString(game.getLevel());
			while (renderWindow.pollEvent(gameEvent)) //���� ���� ������� �� ������������, ������� ���� ������������ ��� �������
			{
				gameOver.processGameEvent(gameEvent, state);
			}
			gameOver.draw();
			renderWindow.display();
			break;
		}

		case GameState::Playing:
		{
			game.processGame(gameEvent, state); // ��������� ����
			while (renderWindow.pollEvent(gameEvent)) //���� ���� ������� �� ������������, ������� ���� ������������ ��� �������
			{
				game.processGameEvent(gameEvent, state); //������������ ������� �� ������������
			}
			game.draw();
			renderWindow.display();
		}	//END CASE

		}	//END SWITCH

	} //END GAME LOOP

	return 0;
}