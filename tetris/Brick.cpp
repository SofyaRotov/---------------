#include "Brick.h"
#include "TetrisFunctions.h"

void Brick::setPreviewPosition()
{
	for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
	{
		coord_t newCoord(getPosition(i).x + 9, getPosition(i).y + 8);
		sf::Vector2f pos = calculateSpritePosition(i, newCoord);
		getSpriteArray()[i].setPosition(pos);
	}
}

void Brick::setActivePosition()
{
	for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
	{
		coord_t newCoord(getPosition(i).x - 9, getPosition(i).y - 8);
		sf::Vector2f pos = calculateSpritePosition(i, newCoord);
		getSpriteArray()[i].setPosition(pos);
	}
}

void Brick::removeSprite(int spriteArrayIndex) //������� �������� �� �������
{
	Sprite* blankSprite = new Sprite();
	getSpriteArray()[spriteArrayIndex] = *blankSprite;
}

void Brick::resetTexture() //��������� ������� �� �����, ��� �� ��������� ������� 
{
	brickTexture.loadFromFile("Images/brick.png");

	for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
	{
		getSpriteArray()[i].setTexture(brickTexture);
	}
}

bool Brick::operator!=(const Brick* rhs)
{
	for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
	{
		const coord_t& position = getPosition(i);
		const coord_t& rhsPosition = rhs->getPosition(i);
		if (position != rhsPosition ||
			brickType != rhs->brickType ||
			directionFacing != rhs->directionFacing)
		{
			return true;
		}
	}

	return false;
}

Brick::Brick(BrickType _brickType)
{
	previewDirectionFacing = NullDirection;
	isLockedBrick = false;
	brickTexture.loadFromFile("Images/brick.png");
	brickType = _brickType;
	isPreviewCalculated = false;
}