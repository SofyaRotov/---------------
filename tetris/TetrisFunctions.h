#ifndef TETFUNC_H
#define TETFUNC_H

#include "Brick.h"
#include "Defines.h"
#include <vector>

using namespace std;


void CheckInput(Event* gameEvent, Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH], unsigned int* score);

bool IsBottomCollision(Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH]);

bool IsSideCollision(Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH], 
						bool checkLeftSide = false, bool checkRightSide = false);

bool IsRotationCollision(Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH]);

void ClearRow(vector<Brick::Ptr>& brickList, int yPosToClear, bool gameBoard[GB_HEIGHT][GB_WIDTH]);

void RefillBrickQueue(vector<Brick::Ptr>& brickQueue);

Brick::Ptr PullBrickFromQueue(vector<Brick::Ptr>& brickQueue);

#endif