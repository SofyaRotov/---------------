#ifndef LINE_H
#define LINE_H


#include "Brick.h"

class SimpleLine : public Brick
{
public:
	SimpleLine()
		: Brick(Brick::Line)
	{
		for (int i = 0; i < SPRITES_IN_BRICK; i++)
		{
			brickSprite[i].setColor(Color(255, 0, 0));		//Red
			directionFacing = Brick::West;

			brickSprite[i].setTexture(brickTexture);
			isSpriteMarkedForDeletion[i] = false;
		}

		setInitialPositions();
	}

	void setInitialPositions()
	{
		setPosition(0, coord_t(3, 2));
		setPosition(1, coord_t(4, 2));
		setPosition(2, coord_t(5, 2));
		setPosition(3, coord_t(6, 2));
	}

	coord_t(&previewRotate())[SPRITES_IN_BRICK]
	{


		switch (directionFacing)
		{
		case Brick::West:
		{
			previewDirectionFacing = Brick::North;
			previewRotateCoords[0] = calculateNewPosition(0, 3, -3);
			previewRotateCoords[1] = calculateNewPosition(1, 2, -2);
			previewRotateCoords[2] = calculateNewPosition(2, 1, -1);
			break;
		}

		case Brick::North:
		{
			previewDirectionFacing = Brick::East;
			previewRotateCoords[0] = calculateNewPosition(0, 3, 3);
			previewRotateCoords[1] = calculateNewPosition(1, 2, 2);
			previewRotateCoords[2] = calculateNewPosition(2, 1, 1);
			break;
		}

		case Brick::East:
		{
			previewDirectionFacing = Brick::South;
			previewRotateCoords[0] = calculateNewPosition(0, -3, 3);
			previewRotateCoords[1] = calculateNewPosition(1, -2, 2);
			previewRotateCoords[2] = calculateNewPosition(2, -1, 1);
			break;
		}

		case Brick::South:
		{
			previewDirectionFacing = Brick::West;
			previewRotateCoords[0] = calculateNewPosition(0, -3, -3);
			previewRotateCoords[1] = calculateNewPosition(1, -2, -2);
			previewRotateCoords[2] = calculateNewPosition(2, -1, -1);
			break;
		}
		}
		previewRotateCoords[3] = getPosition(3);
		isPreviewCalculated = true;
		return previewRotateCoords;
	}
};

#endif