#include <SFML\Audio.hpp>
#include <vector>
#include <list>
#include <iostream>
#include <time.h>

#include "Brick.h"
#include "TetrisFunctions.h"

using namespace std;

void CheckInput(Event* gameEvent, Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH], unsigned int* score)
{
    Keyboard::Key keyPressed = gameEvent->key.code;

    if (keyPressed == Keyboard::Left)
    {	
        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            if (activeBrick->getPosition(i).x <= BRICK_LEFT_BOUNDRY || 
                IsSideCollision(activeBrick, gameBoard, true))
            {
                return;
            }
        }

        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            activeBrick->setPosition(i, activeBrick->calculateNewPosition(i, -1, 0));
        }
    }
    else if (keyPressed == Keyboard::Right)
    {
        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            if (activeBrick->getPosition(i).x >= BRICK_RIGHT_BOUNDRY ||
                IsSideCollision(activeBrick, gameBoard, false, true))
            {
                return;
            }
        }

        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            activeBrick->setPosition(i, activeBrick->calculateNewPosition(i, 1, 0));
        }
    }
    else if (keyPressed == Keyboard::Down)
    {
        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            if (activeBrick->getPosition(i).y == BRICK_LOWER_BOUNDRY ||
                IsBottomCollision(activeBrick, gameBoard))
            {
                activeBrick->lockBrick();
                return;
            }
        }

        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            activeBrick->setPosition(i, activeBrick->calculateNewPosition(i, 0, 1));
        }

        *score += 1;
    }
    else if (keyPressed == Keyboard::Z)
    {	
        SoundBuffer rotateSoundBuffer;
        rotateSoundBuffer.loadFromFile("Sounds/rotate.wav");
        Sound rotateSound;
        rotateSound.setBuffer(rotateSoundBuffer);
        rotateSound.setVolume(50.0f);

        if (!IsRotationCollision(activeBrick, gameBoard))
        {
            rotateSound.play();
            activeBrick->rotate();

            //Keeps the function active long enough to hear the rotate sound play
            //Probably the hackiest thing ever, but it works for the most part :/
            while (rotateSound.getStatus() == SoundSource::Playing)
            {
                continue;
            }
        }
    }
    else if (keyPressed == Keyboard::P)
    {
        for (int i = 0; i < GB_HEIGHT; i++)
        {

            cout << "Row " << i << endl;

            for (int j = 0; j < GB_WIDTH; j++)
            {
                cout << "\tColumn " << j << " Value: " << gameBoard[i][j] << endl;
            }
        }
    }
}

bool IsBottomCollision(Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH])
{
    for (int i = 0; i < SPRITES_IN_BRICK; i++)
    {
        int xPos = activeBrick->getPosition(i).x;
        int yPos = activeBrick->getPosition(i).y;

        if (yPos == BRICK_LOWER_BOUNDRY || gameBoard[yPos + 1][xPos])
        {
            return true;
        }
    }

    return false;
}

bool IsSideCollision(Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH], 
                     bool checkLeftSide, bool checkRightSide)
{
    if (checkLeftSide)
    {
        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            int xPos = activeBrick->getPosition(i).x;
            int yPos = activeBrick->getPosition(i).y;

            if (xPos == BRICK_LEFT_BOUNDRY || gameBoard[yPos][xPos - 1])
            {
                return true;
            }
        }
    }
    else if (checkRightSide)
    {
        for (int i = 0; i < SPRITES_IN_BRICK; i++)
        {
            int xPos = activeBrick->getPosition(i).x;
            int yPos = activeBrick->getPosition(i).y;

            if (xPos == BRICK_RIGHT_BOUNDRY || gameBoard[yPos][xPos + 1])
            {
                return true;
            }
        }
    }

    return false;
}

bool IsRotationCollision(Brick::Ptr activeBrick, bool gameBoard[GB_HEIGHT][GB_WIDTH])
{
    Brick::coord_t (&position)[SPRITES_IN_BRICK] = activeBrick->previewRotate();
    for (size_t i = 0; i < SPRITES_IN_BRICK; i++)
    {
        if (position[i].x < BRICK_LEFT_BOUNDRY || position[i].x > BRICK_RIGHT_BOUNDRY
            || position[i].y > BRICK_LOWER_BOUNDRY
            || gameBoard[position[i].x][position[i].y])
        {
            return true;
        }
    }
    return false;
}

void ClearRow(vector<Brick::Ptr>& brickList, int yPosToClear, bool gameBoard[GB_HEIGHT][GB_WIDTH])
{
    for (int i = 0; i < GB_WIDTH; i++)
    {
        gameBoard[yPosToClear][i] = false;
    }

    for (int i = 0; i < brickList.size(); i++)
    {
        Brick::Ptr currentBrick = brickList.at(i);

        for (int j = 0; j < SPRITES_IN_BRICK; j++)
        {
            if (currentBrick->getPosition(j).y == yPosToClear)
            {
                currentBrick->markSpriteForDeletion(j);
                currentBrick->removeSprite(j);
            }

            else if (currentBrick->getPosition(j).y < yPosToClear && !currentBrick->getSpriteDeletionStatus(j))
            {
                gameBoard[currentBrick->getPosition(j).y][currentBrick->getPosition(j).x] = false;
                currentBrick->setPosition(j, currentBrick->calculateNewPosition(j, 0, 1));
            }
        }
    }

    //CleanupBricks(brickList);

    for (int i = 0; i < brickList.size(); i++)
    {
        Brick::Ptr currentBrick = brickList.at(i);

        for (int j = 0; j < SPRITES_IN_BRICK; j++)
        {
            if (!currentBrick->getSpriteDeletionStatus(j))
            {
                gameBoard[currentBrick->getPosition(j).y][currentBrick->getPosition(j).x] = true;
            }
        }
    }
}

void RefillBrickQueue(vector<Brick::Ptr>& brickQueue)
{
    brickQueue.clear();
    vector<Brick::Ptr> tempQueue;
    Texture brickTex;
    brickTex.loadFromFile("Images/brick.png");
    srand(time(nullptr));

    for (int i = 0; i < BRICKS_IN_QUEUE; i++)
    {
        for (int j = 0; j < NUMBER_OF_BRICK_TYPES; j++)
        {
            for (int k = 0; k < 2; k++)
            {
                Brick::Ptr newBrick = Brick::create((Brick::BrickType)j);
                tempQueue.push_back(newBrick);
                i++;
            }
        }
    }

    while (tempQueue.size() > 0)
    {
        int rng = (double)rand() / (RAND_MAX + 1) * (tempQueue.size() - 1);
        auto iter = tempQueue.begin();
        brickQueue.push_back(tempQueue.at(rng));

        while (iter != tempQueue.end())
        {
            if (*iter != tempQueue.at(rng))
            {
                iter++;
            }
            else
            {
                break;
            }
        }

        tempQueue.erase(iter);
    }

    for (int i = 0; i < brickQueue.size(); i++)
    {
        brickQueue.at(i)->resetTexture();
    }
}

Brick::Ptr PullBrickFromQueue(vector<Brick::Ptr>& brickQueue)
{
    Brick::Ptr newBrick = brickQueue.at(brickQueue.size() - 1);
    brickQueue.pop_back();

    if (brickQueue.size() == 0)
    {
        RefillBrickQueue(brickQueue);
    }

    newBrick->resetTexture();
    return newBrick;
}