#ifndef GAME_PAGE_H
#define GAME_PAGE_H

#include "Page.h"
#include "Game.h"
#include <string>

class GamePage : public Page
{
public:
	GamePage()
		: Page()
	{
		std::shared_ptr<sf::Font> font = std::make_shared<sf::Font>(); //�����
		font->loadFromFile("Images/arial.ttf");
		m_objectsManager.add<sf::Font>("font", font);

		std::shared_ptr<sf::Text> scoreHeaderText = std::make_shared<sf::Text>("Score: ", *font); //����
		std::shared_ptr<sf::Text> scoreDisplayText = std::make_shared<sf::Text>();
		scoreDisplayText->setFont(*font);
		std::shared_ptr<sf::Text> levelHeaderText = std::make_shared<sf::Text>("Level: ", *font); //�������
		std::shared_ptr<sf::Text> levelDisplayText = std::make_shared<sf::Text>();
		levelDisplayText->setFont(*font);
		std::shared_ptr<sf::Text> nextBrickText = std::make_shared<sf::Text>("Next:", *font); //����� �� ��������� �����

		scoreHeaderText->setCharacterSize(TEXT_CHAR_SIZE);
		scoreHeaderText->setPosition(450, 20);
		m_objectsManager.add<sf::Text>("scoreHeaderText", scoreHeaderText);
		scoreDisplayText->setCharacterSize(TEXT_CHAR_SIZE);
		scoreDisplayText->setPosition(520, 20);
		m_objectsManager.add<sf::Text>("scoreDisplayText", scoreDisplayText);
		levelHeaderText->setCharacterSize(TEXT_CHAR_SIZE);
		levelHeaderText->setPosition(450, 60);
		m_objectsManager.add<sf::Text>("levelHeaderText", levelHeaderText);
		levelDisplayText->setCharacterSize(TEXT_CHAR_SIZE);
		levelDisplayText->setPosition(520, 60);
		m_objectsManager.add<sf::Text>("levelDisplayText", levelDisplayText);
		nextBrickText->setCharacterSize(TEXT_CHAR_SIZE);
		nextBrickText->setPosition(450, 120);
		m_objectsManager.add<sf::Text>("nextBrickText", nextBrickText);

		std::shared_ptr<sf::Texture> backgroundTexture = std::make_shared<sf::Texture>(); //���
		backgroundTexture->loadFromFile("Images/background.png");
		m_objectsManager.add<sf::Texture>("background", backgroundTexture);

		std::shared_ptr<sf::Texture> brickPreviewBackground = std::make_shared<sf::Texture>(); //��� ������
		brickPreviewBackground->loadFromFile("Images/previewBG.png");
		m_objectsManager.add<sf::Texture>("brickPreview", brickPreviewBackground);


		std::shared_ptr<sf::Sprite> backgroundSprite = std::make_shared<sf::Sprite>(); //��� ��������� ���� (������ ���������)
		backgroundSprite->setTexture(*backgroundTexture);
		m_objectsManager.add<sf::Sprite>("backgroundSprite", backgroundSprite);

		std::shared_ptr<sf::Sprite> brickPreviewSprite = std::make_shared<sf::Sprite>(); //��� ���� ������
		brickPreviewSprite->setTexture(*brickPreviewBackground);
		brickPreviewSprite->setPosition(450, 150);
		m_objectsManager.add<sf::Sprite>("brickPreviewSprite", brickPreviewSprite);
	}

	void draw()
	{
		if (!m_renderWindow) return;

		getObject<sf::Text>("scoreDisplayText")->setString(getScore()); //���-�� ����� �� ������� ������
		getObject<sf::Text>("levelDisplayText")->setString(getLevel()); //������� �� ������� ������
		m_renderWindow->clear(Color(0, 255, 255));
		//������������ 
		m_renderWindow->draw(*getObject<sf::Sprite>("backgroundSprite"));
		m_renderWindow->draw(*getObject<sf::Sprite>("brickPreviewSprite"));
		m_renderWindow->draw(*getObject<sf::Text>("nextBrickText"));
		m_renderWindow->draw(*getObject<sf::Text>("scoreHeaderText"));
		m_renderWindow->draw(*getObject<sf::Text>("scoreDisplayText"));
		m_renderWindow->draw(*getObject<sf::Text>("levelHeaderText"));
		m_renderWindow->draw(*getObject<sf::Text>("levelDisplayText"));

		for (const auto& brick : m_logic.getBricks()) //��� ��������� ������� �� ������� ���� (�����)
		{
			for (int i = 0; i < SPRITES_IN_BRICK; i++)
			{
				m_renderWindow->draw(brick->getSpriteArray()[i]);
			}
		}
		for (int i = 0; i < SPRITES_IN_BRICK; i++) //��� ��������� ��������� ������ � ���������� 
		{
			m_renderWindow->draw(m_logic.getActiveBrick()->getSpriteArray()[i]);
			m_renderWindow->draw(m_logic.getPreviewBrick()->getSpriteArray()[i]);
		}
	}

	void processGame(const sf::Event& gameEvent, GameState& state) //������� ������
	{
		m_logic.processGame(state);
	}

	void processGameEvent(const sf::Event& gameEvent, GameState& state)
	{
		if (!m_renderWindow) return;
		if (gameEvent.type == Event::Closed || gameEvent.key.code == Keyboard::Escape)
		{
			m_renderWindow->close();
		}
		if (gameEvent.type == Event::KeyPressed) //���� ������ �� ����������, �� ����� ��� � ������� ������ ��� ���������
		{
			m_logic.checkInput(gameEvent);
		}
	}

	std::string getScore() //�������� �� �� ���-�� ����� � ���� �������
	{
		return std::to_string(m_logic.getScore());

	}
	std::string getLevel() //������� �� ��
	{
		return std::to_string(m_logic.getLevel());
	}
private:
	TetrisLogic m_logic;  //������� ������

};


#endif