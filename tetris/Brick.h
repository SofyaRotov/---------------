#ifndef BRICK_H
#define BRICK_H

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include "Defines.h"
#include <memory>

using namespace sf;
using namespace std;

class Brick
{
public:
	typedef std::shared_ptr<Brick> Ptr;

	enum BrickType
	{
		Box,
		Line,
		Z,
		ReverseZ,
		L,
		ReverseL,
		T
	};

	enum DirectionType
	{
		North,
		South,
		East,
		West,
		NullDirection
	};

	struct coord_t
	{
		coord_t()
			: x(0)
			, y(0)
		{}
		coord_t(size_t _x, size_t _y)
			: x(_x)
			, y(_y)
		{}
		int x;
		int y;

		bool operator != (const coord_t& rhs) const
		{
			return x != rhs.x || y != rhs.y;
		}
	};

protected:
	Sprite brickSprite[SPRITES_IN_BRICK];
	BrickType brickType;
	DirectionType directionFacing;
	DirectionType previewDirectionFacing;
	coord_t positionCoords[SPRITES_IN_BRICK];
	coord_t previewRotateCoords[SPRITES_IN_BRICK];
	bool isLockedBrick;
	bool isSpriteMarkedForDeletion[SPRITES_IN_BRICK];
	bool isPreviewCalculated;
	Texture brickTexture;

public:
	virtual coord_t(&previewRotate())[SPRITES_IN_BRICK] = 0;

	virtual void rotate()
	{
		if (!isPreviewCalculated)
		{
			previewRotate();
		}
		directionFacing = previewDirectionFacing;
		setPosition(0, previewRotateCoords[0]);
		setPosition(1, previewRotateCoords[1]);
		setPosition(2, previewRotateCoords[2]);
		setPosition(3, previewRotateCoords[3]);
		isPreviewCalculated = false;
	}

	coord_t calculateNewPosition(size_t brickIndex, size_t xOffset, size_t yOffset) //����������� ����� ���������� ��� ������� �� ��������� ������
	{
		coord_t position(positionCoords[brickIndex].x + xOffset, positionCoords[brickIndex].y + yOffset);
		return position;
	}

	void setPosition(size_t brickIndex, coord_t& position) //��������� �������
	{
		positionCoords[brickIndex] = position;
		sf::Vector2f pos = calculateSpritePosition(brickIndex, position);
		getSpriteArray()[brickIndex].setPosition(pos);
	}

	const coord_t& getPosition(size_t brickIndex) const
	{
		return positionCoords[brickIndex];
	}

	const coord_t(&getPosition())[SPRITES_IN_BRICK] //���������� ������ ���������, ��������������� ���� ��������� ������
	{
		return positionCoords;
	}

	static Brick::Ptr create(BrickType); //������ ���������� ��� ������

	virtual void lockBrick()
	{
		isLockedBrick = true;  //������ ����� � ����������� 
	}

	virtual bool isLocked()
	{
		return isLockedBrick;
	}

	virtual Sprite* getSpriteArray()
	{
		return brickSprite; //������ �� �������� ��������� 
	}



	virtual BrickType getBrickType()
	{
		return this->brickType;  //��� ������
	}

	virtual bool getSpriteDeletionStatus(int spriteArrayIndex)
	{
		return isSpriteMarkedForDeletion[spriteArrayIndex]; //�������� �� �������� �������� 
	}

	virtual void setPreviewPosition();

	virtual void setActivePosition();

	virtual void removeSprite(int spriteArrayIndex);

	virtual void resetTexture();

	virtual bool operator!=(const Brick* brick);

	virtual void markSpriteForDeletion(int spriteArrayIndex)
	{
		isSpriteMarkedForDeletion[spriteArrayIndex] = true;  //�������� �������� ������ ������
	}

	Brick(BrickType brickType);

	virtual ~Brick()
	{}

private:
	sf::Vector2f calculateSpritePosition(size_t brickIndex, coord_t& position) //������� ��� ������ � ����������� �� �� ����� �� ������� ����, �������� ������� �������
	{
		float yPos = float(position.y * float(getSpriteHeight(brickIndex)));
		float xPos = float((position.x + GB_X_OFFSET) * float(getSpriteWidth(brickIndex)));
		return sf::Vector2f(xPos, yPos);
	}
	//���������� ������� �������
	virtual size_t getSpriteHeight(size_t spriteArrayIndex) //������
	{
		return getSpriteArray()[spriteArrayIndex].getTexture()->getSize().y;
	}

	virtual size_t getSpriteWidth(size_t spriteArrayIndex) //������
	{
		return getSpriteArray()[spriteArrayIndex].getTexture()->getSize().x;
	}



};

#endif