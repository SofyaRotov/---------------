#ifndef DEFINES_H // ��� �� ��� ���������� �������� 
#define DEFINES_H

const size_t SCREEN_WIDTH = 640;	//������	
const size_t SCREEN_HEIGHT = 480;	//������
const size_t TEXT_CHAR_SIZE = 20;  //������ ����

const size_t TETRIS_CLEAR_SCORE = 1200; //�� ������� ���� ��������� 
const size_t MULTI_LINE_CLEAR_SCORE = 200; //���� ��������� ����� ����������
const size_t SINGLE_LINE_CLEAR_SCORE = 100;// �� ���� ��������� �����
const size_t BRICK_DROP_SCORE_RATE = 1;  //���-�� ����� �� ������� �������
const size_t BRICK_LOCK_SCORE = 15;   //���-�� ����� �� ��������������� ������

const size_t BRICK_LEFT_BOUNDRY = 0; //����� �������
const size_t BRICK_RIGHT_BOUNDRY = 9;	// ������
const size_t BRICK_LOWER_BOUNDRY = 22; // ������
const size_t BRICK_UPPER_BOUNDRY = 3; //�������
const size_t SPRITES_IN_BRICK = 4;    // ������ ������� �� 4� ��-��
const size_t BRICK_FALL_RATE = 1; //�������� �� �������� ������� (� ������� �������������)
const size_t NUMBER_OF_BRICK_TYPES = 7; //���-�� ��������� �������
const size_t BRICKS_IN_QUEUE = NUMBER_OF_BRICK_TYPES * 2; // ���-�� ������� � �������
const size_t LEVEL_MAX = 25; //������������ ������� � ����

const size_t GB_X_OFFSET = 11; //����� �� �������� ���� 
const size_t GB_WIDTH = 10; //������ �������� ����
const size_t GB_HEIGHT = 23; // ������

enum GameState  //���-�� ������ ������ � ���� (enum = ������������)
{
	MainMenu, //������� ����
	Playing, //������� �����
	GameOver //����� ����
};

#endif