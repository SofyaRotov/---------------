#ifndef MENU_PAGE_H
#define MENU_PAGE_H

#include "Page.h"

class MenuPage : public Page
{
public:
	MenuPage()
		: Page() //�������� ����������� � ������������� ������ 
	{
		std::shared_ptr<sf::Font> font = std::make_shared<sf::Font>(); //������ ����� 
		font->loadFromFile("Images/arial.ttf"); //��������� ��� �� �����
		m_objectsManager.add<sf::Font>("font", font); //��������� ��� �� ���������

		std::shared_ptr<sf::Text> mainMenuHeaderText = std::make_shared<sf::Text>("SFML TETRIS!", *font); //������ ��������� ����
		std::shared_ptr<sf::Text> mainMenuStartText = std::make_shared<sf::Text>("Start", *font);
		std::shared_ptr<sf::Text> mainMenuQuitText = std::make_shared<sf::Text>("Quit", *font);
		mainMenuHeaderText->setCharacterSize(40); //������ �������� ��� ���������
		mainMenuHeaderText->setPosition(180, 50); //������� ���������
		m_objectsManager.add<sf::Text>("headerText", mainMenuHeaderText); //��������� ��� �� ���������
		mainMenuStartText->setCharacterSize(32); //������ ������� ��� ������ 
		mainMenuStartText->setPosition(285, 210); // �������
		m_objectsManager.add<sf::Text>("startText", mainMenuStartText); //��������� ��� �� ���������
		mainMenuQuitText->setCharacterSize(32);  //������� ��� �������� 
		mainMenuQuitText->setPosition(285, 310); //������� 
		m_objectsManager.add<sf::Text>("quitText", mainMenuQuitText); //��������� ��� �� ���������

		std::shared_ptr<sf::Texture> mainMenuBackground = std::make_shared<sf::Texture>(); //��������� ��� ��� �������� ����
		mainMenuBackground->loadFromFile("Images/mainMenuBG.png"); //��������� ��� �� �����
		m_objectsManager.add<sf::Texture>("background", mainMenuBackground); ////��������� ��� �� ���������

		std::shared_ptr<sf::Sprite> mainMenuBackgroundSprite = std::make_shared<sf::Sprite>(); //������ ��������� ��� ���� 
		mainMenuBackgroundSprite->setTexture(*mainMenuBackground); //������������� ���, ����������� � ����� 
		m_objectsManager.add<sf::Sprite>("backgroundSprite", mainMenuBackgroundSprite); ////��������� ��� �� ���������
	}

	void draw()
	{
		//���� �� ���������� ������, �� �������
		if (!m_renderWindow) return;
		//������������ ��� � ������
		m_renderWindow->draw(*getObject<sf::Sprite>("backgroundSprite"));
		m_renderWindow->draw(*getObject<sf::Text>("startText"));
		m_renderWindow->draw(*getObject<sf::Text>("quitText"));
		m_renderWindow->draw(*getObject<sf::Text>("headerText"));
	}

	void processGameEvent(const sf::Event& gameEvent, GameState& state) //�������, ������� ������������ ������� �� ������������
	{
		if (!m_renderWindow) return;
		if (gameEvent.type == sf::Event::Closed || gameEvent.key.code == sf::Keyboard::Escape) //���� �� ������� ������ 
		{
			m_renderWindow->close();
		}
		else if (gameEvent.type == sf::Event::MouseButtonPressed) //���� �� ������ ������
		{
			sf::Vector2i mousePos = sf::Mouse::getPosition(*m_renderWindow); //������� ������� (���� ������)

			if (m_objectsManager.getObject<sf::Text>("startText")->getGlobalBounds().contains(sf::Vector2f(mousePos))) //��� ������
			{
				state = GameState::Playing; //����������� ����� ���� � �������
			}
			if (m_objectsManager.getObject<sf::Text>("quitText")->getGlobalBounds().contains(sf::Vector2f(mousePos))) //��� ������
			{
				m_renderWindow->close(); // �������
			}
		}
	}


};

#endif